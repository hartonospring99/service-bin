FROM openjdk:19-jdk-alpine
EXPOSE 8081
ADD target/gateway-bin.jar gateway-bin.jar
ENTRYPOINT ["java", "-jar", "/gateway-bin.jar"]

