package com.hartono.gatewaybinchecker.util;

import com.hartono.gatewaybinchecker.exception.ThrowableException;
import com.hartono.gatewaybinchecker.exception.ValidationApiException;
import io.vertx.core.json.JsonObject;
import jakarta.ws.rs.core.NoContentException;

import java.time.LocalDateTime;

public class ExceptionUtil {
	private ExceptionUtil(){
		//do nothing
	}

	public static Object ExceptionExceptionFormatter(String uri, LocalDateTime reqAt, JsonObject reqBody,
			JsonObject respBody, Exception ex, Object expType) {
		return ExceptionExceptionFormatter(uri, reqAt, reqBody, LocalDateTime.now(), respBody, ex, expType);
	}

	public static Object ExceptionExceptionFormatter(String uri, LocalDateTime reqAt, JsonObject reqBody,
			LocalDateTime respAt, JsonObject respBody, Exception ex, Object expType) {
		JsonObject exceptionBody = LoggingUtil.logApiFormatter(uri, reqAt, reqBody, respAt, respBody, ex);
		if (expType == com.hartono.gatewaybinchecker.exception.ValidationApiException.class)
			return new ValidationApiException(exceptionBody.encode());
		else if (expType == ThrowableException.class)
			return new ThrowableException(exceptionBody.encode());
		else if (expType == NoContentException.class)
			return new NoContentException(exceptionBody.encode());
		else
			return new Exception(exceptionBody.encode());
	}

	public static String exceptionStandard(JsonObject obj) {

		StringBuilder str = new StringBuilder();
		str.append("uri: " + obj.getString("uri"));
		str.append("\n");
		str.append("request body: ");
		str.append(obj.containsKey("reqBody") == false ? "" : obj.getJsonObject("reqBody").encodePrettily());
		str.append("\n");
		str.append("request at: " + obj.getString("reqAt"));
		str.append("\n");
		str.append("response body: ");
		str.append(obj.containsKey("respBody") == false ? "" : obj.getJsonObject("respBody").encodePrettily());
		str.append("\n");
		str.append("response at: " + obj.getString("respAt"));
		str.append("\n");
		if (obj.containsKey("stackTrace"))
			str.append(obj.getString("stackTrace"));

		return str.toString();

	}

}
