package com.hartono.gatewaybinchecker.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	private DateUtil(){
		// do Nothing
	}
	static final long ONE_MINUTE_IN_MILLIS = 60000;
	static final long ONE_SECOND_IN_MILLIS = 1000;

	private static final String GENERIC_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String SHORT_FORMAT = "dd-MMM-yy";

	public static Date convertFromStringToDate(String dateFrom) throws ParseException {
		SimpleDateFormat genericFormat = new SimpleDateFormat(GENERIC_FORMAT);
		return genericFormat.parse(dateFrom);
	}

	public static LocalDateTime convertFromStringToLocalDateTime(String dateFrom) throws ParseException {
		try {
			SimpleDateFormat genericFormat = new SimpleDateFormat(GENERIC_FORMAT);
			return genericFormat.parse(dateFrom).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		} catch (ParseException e) {
			SimpleDateFormat genericFormat = new SimpleDateFormat(GENERIC_FORMAT);
			return genericFormat.parse(dateFrom.replace("T", " ").substring(0, 19)).toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDateTime();
		}
	}

	public static LocalDateTime convertFromStringToLocalDateTime(String dateFrom, String pattern)
			throws ParseException {
		try {
			SimpleDateFormat genericFormat = new SimpleDateFormat(pattern);
			return genericFormat.parse(dateFrom).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		} catch (ParseException e) {
			SimpleDateFormat genericFormat = new SimpleDateFormat(pattern);
			return genericFormat.parse(dateFrom.replace("T", " ")).toInstant().atZone(ZoneId.systemDefault())
					.toLocalDateTime();
		}
	}

	public static LocalDateTime convertFromStringToLocalDateTime(String dateFrom, String pattern,
			boolean checkNullValue) throws ParseException {
		try {
			if (checkNullValue && dateFrom == null)
				return null;
			SimpleDateFormat genericFormat = new SimpleDateFormat(pattern);
			return genericFormat.parse(dateFrom).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		} catch (ParseException e) {
			SimpleDateFormat genericFormat = new SimpleDateFormat(pattern);
			return genericFormat.parse(dateFrom.replace("T", " ")).toInstant().atZone(ZoneId.systemDefault())
					.toLocalDateTime();
		}
	}

	public static LocalDateTime convertFromDateToLocalDateTime(Date dateFrom) {
		return dateFrom.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static String convertFromLocalDateTimeToString(LocalDateTime dateFrom) {
		SimpleDateFormat sdf = new SimpleDateFormat(GENERIC_FORMAT);
		return sdf.format(Date.from(dateFrom.atZone(ZoneId.systemDefault()).toInstant()));
	}

	public static String convertFromLocalDateTimeToString(LocalDateTime dateFrom, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(Date.from(dateFrom.atZone(ZoneId.systemDefault()).toInstant()));
	}

	public static String convertLocalDateTimeToOffsetDateTimeToString(LocalDateTime datetime, String format) {
		ZoneOffset offset = ZoneId.systemDefault().getRules().getOffset(datetime);
		OffsetDateTime offDateTime = datetime.atOffset(offset);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		return offDateTime.format(dtf);
	}

	public static LocalDateTime convertOffsetDateTimeInStringToLocalDateTime(String strDateTime, String format) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		return LocalDateTime.parse(strDateTime, dtf);
	}

	public static Date addSecondsToDate(Date date, long additionalSeconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		long t = cal.getTimeInMillis();
		return new Date(t + (additionalSeconds * ONE_SECOND_IN_MILLIS));
	}

	public static Date addMinutesToDate(Date date, long additionalMinutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		long t = cal.getTimeInMillis();
		return new Date(t + (additionalMinutes * ONE_MINUTE_IN_MILLIS));
	}

	public static Date addYearsToDate(Date date, int additionalYears) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, additionalYears);
		return cal.getTime();
	}

	public static Date subtractMonthToDate(Date date, int monthReduction) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -monthReduction);
		return cal.getTime();
	}

	public static LocalDateTime convertFromStringddmmmyyToLocalDateTime(String dateFrom) throws ParseException {
		try {
			SimpleDateFormat ddmmmyy = new SimpleDateFormat(SHORT_FORMAT);
			return ddmmmyy.parse(dateFrom).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		} catch (ParseException e) {
			SimpleDateFormat ddmmmyy = new SimpleDateFormat(SHORT_FORMAT);
			return ddmmmyy.parse(dateFrom.replace("T", " ").substring(0, 19)).toInstant().atZone(ZoneId.systemDefault())
					.toLocalDateTime();
		}
	}

	public static LocalDateTime convertFromStringmmmyyToLocalDateTime(String dateFrom) throws ParseException {
		try {
			SimpleDateFormat ddmmmyy = new SimpleDateFormat(SHORT_FORMAT);
			return ddmmmyy.parse("01-" + dateFrom).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		} catch (ParseException e) {
			SimpleDateFormat ddmmmyy = new SimpleDateFormat(SHORT_FORMAT);
			return ddmmmyy.parse(dateFrom.replace("T", " ").substring(0, 19)).toInstant().atZone(ZoneId.systemDefault())
					.toLocalDateTime();
		}
	}

	public static String getIndonesianDate(LocalDateTime date) {
		String[] month = new String[] { "", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus",
				"September", "Oktober", "November", "Desember" };

		return "" + date.getDayOfMonth() + " " + month[date.getMonthValue()] + " " + date.getYear();
	}

	public static String getIndonesianMonth(LocalDateTime date) {
		String[] month = new String[] { "", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus",
				"September", "Oktober", "November", "Desember" };

		return "" + month[date.getMonthValue()] + " " + date.getYear();
	}

	public static String convertFromLocalDateTimeToStringType2(LocalDateTime dateFrom) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
		return sdf.format(Date.from(dateFrom.atZone(ZoneId.systemDefault()).toInstant()));
	}

	public static LocalDateTime convertFromStringYYYYMMDDToLocalDateTime(String dateFrom) throws ParseException {
		SimpleDateFormat formatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		return formatYYYYMMDD.parse(dateFrom).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}
}
