package com.hartono.gatewaybinchecker.util;

import com.hartono.gatewaybinchecker.constant.ConstantVariable;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;

import java.time.LocalDateTime;

public class LoggingUtil {
	private LoggingUtil() {
		// do nothing
	}
	public static void logApiService(Logger logger, String uri, LocalDateTime reqAt, JsonObject reqBody, LocalDateTime respAt,
			JsonObject respBody, Exception ex) {
		logger.info(logJsonToString(logApiFormatter(uri, reqAt, reqBody, respAt, respBody, ex)));
	}

	public static String logJsonToString(JsonObject obj) {
		StringBuilder str = new StringBuilder();
		str.append("uri: " + obj.getString("uri"));
		str.append("\n");
		str.append("request body: ");
		str.append(!obj.containsKey("reqBody") ? "" : obj.getJsonObject("reqBody").encodePrettily());
		str.append("\n");
		str.append("request at: " + obj.getString("reqAt"));
		str.append("\n");
		str.append("response body: ");
		str.append(!obj.containsKey("respBody") ? "" : obj.getJsonObject("respBody").encodePrettily());
		str.append("\n");
		str.append("response at: " + obj.getString("respAt"));
		str.append("\n");
		if (obj.containsKey("stackTrace"))
			str.append(obj.getString("stackTrace"));
		return str.toString();
	}

	public static JsonObject logApiFormatter(String uri, LocalDateTime reqAt, JsonObject reqBody, LocalDateTime respAt,
			JsonObject respBody, Exception ex) {
		JsonObject body = new JsonObject();
		body.put("uri", uri);
		if (reqBody != null && !reqBody.isEmpty())
			body.put("reqBody", reqBody);
		body.put("reqAt", DateUtil.convertFromLocalDateTimeToString(reqAt, ConstantVariable.DATETIME_FULL_FORMAT));
		if (respBody != null && !respBody.isEmpty())
			body.put("respBody", respBody);
		body.put("respAt", DateUtil.convertFromLocalDateTimeToString(respAt, ConstantVariable.DATETIME_FULL_FORMAT));
		if (ex != null)
			body.put("stackTrace", ExceptionUtils.getStackTrace(ex));
		return body;
	}

}
