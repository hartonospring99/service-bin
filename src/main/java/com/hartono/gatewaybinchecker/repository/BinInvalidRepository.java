package com.hartono.gatewaybinchecker.repository;

import com.hartono.gatewaybinchecker.model.BinInvalid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface BinInvalidRepository extends JpaRepository<BinInvalid, String> {
    Optional<BinInvalid> findByBin(String bin);
}
