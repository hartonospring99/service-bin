package com.hartono.gatewaybinchecker.repository;

import com.hartono.gatewaybinchecker.model.BinCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

public interface BinRepository extends JpaRepository<BinCode, String> {

    List<BinCode> findAll();
    Optional<BinCode> findByBin(String bin);
}
