package com.hartono.gatewaybinchecker.controller;


import com.hartono.gatewaybinchecker.exception.ThrowableException;
import com.hartono.gatewaybinchecker.model.BinCode;
import com.hartono.gatewaybinchecker.repository.BinRepository;
import com.hartono.gatewaybinchecker.services.BinCodeService;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.vertx.core.json.JsonObject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BinCodeController {

    @Autowired
    BinCodeService binCodeService;

    @Autowired
    BinRepository binRepository;

    @GetMapping(value = "/v1/bin/{bin}")
    ResponseEntity<?> findBinCode(@Context UriInfo uriInfo,
                               @PathVariable("bin") String bin)
            throws ThrowableException,
            ServerErrorException,
            UnirestException {
        Map<String, Object> respose = new HashMap<>();
        JsonObject data = binCodeService.getBinCode(uriInfo, bin);
        respose.putAll(data.getMap());
        return new ResponseEntity<>(respose,null,HttpStatus.OK);
    }

    @GetMapping(value = "/v1/get")
    ResponseEntity<?> getAll()
            throws ThrowableException,
            ServerErrorException,
            UnirestException {

        Map<String, Object> respose = new HashMap<>();
        List<BinCode> data = binCodeService.findAll();
        respose.put("data",data);

        return new ResponseEntity<>(respose,null,HttpStatus.OK);
    }
}
