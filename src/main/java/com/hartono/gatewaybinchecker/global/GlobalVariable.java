package com.hartono.gatewaybinchecker.global;

import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GlobalVariable {


    public static HashMap<String, JsonObject> binCodeHashMap;
    static {
        binCodeHashMap = new HashMap<>();
    }

    public static List<String> invalidBin;
    static {
        invalidBin = new ArrayList<>();
    }
}
