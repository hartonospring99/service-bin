package com.hartono.gatewaybinchecker.services;

import com.hartono.gatewaybinchecker.model.BinCode;
import com.hartono.gatewaybinchecker.model.BinInvalid;
import com.hartono.gatewaybinchecker.repository.BinInvalidRepository;
import com.hartono.gatewaybinchecker.repository.BinRepository;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;

import static com.hartono.gatewaybinchecker.global.GlobalVariable.binCodeHashMap;
import static com.hartono.gatewaybinchecker.global.GlobalVariable.invalidBin;

@Service
public class InitBin {

    private BinRepository binRepository;
    private BinInvalidRepository binInvalidRepository;

    public InitBin(BinRepository binRepository, BinInvalidRepository binInvalidRepository) {
        this.binRepository = binRepository;
        this.binInvalidRepository = binInvalidRepository;
    }

    public void initBinValid() {
        List<BinCode> allBinValid = binRepository.findAll();
        binCodeHashMap = allBinValid
                .stream()
                .collect(HashMap::new,
                        (map, streamValue) -> map.put(String.valueOf(map.get("bin")), streamValue),
                        (map, map2) -> {
                        });

        /*for (int i = 0; i < allBinValid.size(); i++) {
            String idBin = allBinValid.get(i).getBin();
            JsonObject binJson = JsonObject.mapFrom(allBinValid.get(i));
            binCodeHashMap.put(idBin, binJson);
            System.out.println("JJJJJJJJJJJJ "+binJson);
        }*/
    }

    public void initBinInValid() {
        List<BinInvalid> allBinInValid = binInvalidRepository.findAll();

        for (int i = 0; i < allBinInValid.size(); i++) {
            String idBin = allBinInValid.get(i).getBin();
            invalidBin.add(idBin);
        }
    }
}
