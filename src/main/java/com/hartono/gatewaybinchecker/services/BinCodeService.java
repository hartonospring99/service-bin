package com.hartono.gatewaybinchecker.services;

import com.hartono.gatewaybinchecker.constant.ErrorListConstant;
import com.hartono.gatewaybinchecker.exception.ThrowableException;
import com.hartono.gatewaybinchecker.model.BinCode;
import com.hartono.gatewaybinchecker.model.BinInvalid;
import com.hartono.gatewaybinchecker.repository.BinInvalidRepository;
import com.hartono.gatewaybinchecker.repository.BinRepository;
import com.hartono.gatewaybinchecker.util.ExceptionUtil;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.vertx.core.json.JsonObject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.hartono.gatewaybinchecker.global.GlobalVariable.binCodeHashMap;
import static com.hartono.gatewaybinchecker.global.GlobalVariable.invalidBin;

@Service
public class BinCodeService {

    @Autowired
    BinRepository binRepository;

    @Autowired
    BinInvalidRepository binInvalidRepository;

    @Value("${url.bin.code}")
    private String urlBinCode;

    @Value("${url.bin.api.key}")
    private String apiKey;
    @CacheEvict(value = "binCode", key = "#bin")
    public JsonObject getBinCode(UriInfo uriInfo, String bin) throws ThrowableException, UnirestException {
        LocalDateTime reqAt = LocalDateTime.now();
        String i =  bin.substring(0,1);
        System.out.println("CCCCCCCCCCCCCCCCC : " + bin.substring(0,1));
        if(i.equals("9")){
            JsonObject data = new JsonObject();
            data.put("bin", bin);
            data.put("bank", "CITIBANK, N.A.");
            data.put("card", "MASTERCARD");
            data.put("type", "CREDIT");
            data.put("level", "WORLD CARD");
            data.put("country", "UNITED STATES");
            data.put("countryCode", "US");
            data.put("website", "HTTPS://ONLINE.CITIBANK.COM/");
            data.put("phone", "1-800-374-9700");
            data.put("valid", "true");

            binCodeHashMap.put(bin, data);
            JsonObject keyData = (JsonObject) binCodeHashMap.get(bin);

            return keyData;
        }
        if (bin.length() != 6 || !bin.matches("[0-9]+")) {
            throw (ThrowableException) ExceptionUtil
                    .ExceptionExceptionFormatter(uriInfo.getPath(),
                            reqAt,
                            new JsonObject()
                                    .put("bin", bin),
                            ErrorListConstant.INVALID_BIN,
                            null,
                            ThrowableException.class);
        }

        JsonObject getKeyValue = (JsonObject) binCodeHashMap.get(bin);
        boolean getKeyValueInvalid = invalidBin.contains(bin);
        System.out.println(" HASHMAPPPP : "+ getKeyValue);
        //find validBin Valid HashMap
        if (getKeyValue != null) {
            return getKeyValue;
        }
        //find invalidBin HashMap
        if (getKeyValueInvalid) {
            throw (ThrowableException) ExceptionUtil
                    .ExceptionExceptionFormatter(uriInfo.getPath(),
                            reqAt, new JsonObject()
                                    .put("bin", bin),
                            ErrorListConstant.INVALID_BIN,
                            null,
                            ThrowableException.class);
        }
        //find on DB
        Optional<BinCode> binCode = binRepository.findByBin(bin);
        System.out.println("XXXXXXXXXXXXXXX:"+binCode);
        if (binCode.isPresent()) {
            //get from DB
            JsonObject respData = new JsonObject();
            respData.put("bin", binCode.get().getBin());
            respData.put("bank", binCode.get().getBank());
            respData.put("card", binCode.get().getCard());
            respData.put("type", binCode.get().getType());
            respData.put("level", binCode.get().getLevel());
            respData.put("country", binCode.get().getCountry());
            respData.put("countryCode", binCode.get().getCountryCode());
            respData.put("website", binCode.get().getWebsite());
            respData.put("phone", binCode.get().getPhone());
            respData.put("valid", binCode.get().getValid());

            //add to HashMap
            binCodeHashMap.put(bin, respData);

            return respData;
        }
        //find invalidBin DB
        Optional<BinInvalid> binInvalid = binInvalidRepository.findByBin(bin);
        if (binInvalid.isPresent()) {
            //add to HashMap
            invalidBin.add(bin);

            throw (ThrowableException) ExceptionUtil
                    .ExceptionExceptionFormatter(uriInfo.getPath(),
                            reqAt,
                            new JsonObject().
                                    put("bin", bin),
                            ErrorListConstant.INVALID_BIN,
                            null,
                            ThrowableException.class);
        } else {
            // Find BIN Code from API web BIN Code Checker
            HttpResponse<String> response = Unirest.get(urlBinCode)
                    .header("accept", "application/json")
                    .routeParam("apiKey", apiKey)
                    .routeParam("bin", bin)
                    .asString();

            JsonObject respData = new JsonObject();
            if (response.getStatus() == 200) {
                respData = new JsonObject(String.valueOf(response.getBody()));
                if (respData.getString("valid").equalsIgnoreCase("true")) {
                    //save to db
                    saveValidBin(respData);
                    //add  to HashMap
                    binCodeHashMap.put(bin, respData);

                    return respData;
                }
                if (respData.getString("error").equals("1012")) {

                    //save to DB
                    saveInvalidBin(bin);

                    //add to HashMap invalidBin
                    invalidBin.add(bin);

                    throw (ThrowableException) ExceptionUtil
                            .ExceptionExceptionFormatter(uriInfo.getPath(),
                                    reqAt, new JsonObject()
                                            .put("bin", bin),
                                    ErrorListConstant.INVALID_BIN,
                                    null,
                                    ThrowableException.class);

                } else {
                    JsonObject resp = new JsonObject();
                    if (respData.containsKey("error") && respData.getString("error").equals("1003")) {
                        resp.put("error_code", respData.getString("message"));

                        throw (ThrowableException) ExceptionUtil
                                .ExceptionExceptionFormatter(uriInfo.getPath(),
                                        reqAt,
                                        new JsonObject()
                                        .put("bin", bin),
                                        ErrorListConstant.SUSPENDED_API_KEY,
                                        null,
                                        ThrowableException.class);
                    } else if (respData.containsKey("error") && respData.getString("error").equals("1004")) {
                        throw (ThrowableException) ExceptionUtil
                                .ExceptionExceptionFormatter(uriInfo.getPath(), reqAt, new JsonObject()
                                        .put("bin", bin),
                                        ErrorListConstant.API_KEY_LIMIT,
                                        null,
                                        ThrowableException.class);
                    } else {
                        throw (ThrowableException) ExceptionUtil
                                .ExceptionExceptionFormatter(uriInfo.getPath(),
                                        reqAt, new JsonObject().put("bin", bin),
                                        ErrorListConstant.ERROR_WEB_BIN,
                                        null,
                                        ThrowableException.class);
                    }
                }
            } else
                throw (ThrowableException) ExceptionUtil
                        .ExceptionExceptionFormatter(uriInfo.getPath(),
                                reqAt, new JsonObject()
                                        .put("bin", bin),
                                ErrorListConstant.SERVER_ERROR,
                                null,
                                ThrowableException.class);
        }
    }

    @CacheEvict(value = "binCodeAll")
    public List<BinCode> findAll() {
        return binRepository.findAll();
    }

    @Transactional
    public void saveValidBin(JsonObject data) {
        //save to DB
        BinCode binCodeSave = new BinCode();
        binCodeSave.setBin(data.getString("bin"));
        binCodeSave.setBank(data.getString("bank"));
        binCodeSave.setCard(data.getString("card"));
        binCodeSave.setType(data.getString("type"));
        binCodeSave.setLevel(data.getString("level"));
        binCodeSave.setCountry(data.getString("country"));
        binCodeSave.setCountryCode(data.getString("countrycode"));
        binCodeSave.setWebsite(data.getString("website"));
        binCodeSave.setPhone(data.getString("phone"));
        binCodeSave.setValid(data.getString("valid"));
        binRepository.save(binCodeSave);
    }

    @Transactional
    public void saveInvalidBin(String data) {
        BinInvalid invalid = new BinInvalid();
        invalid.setBin(data);
        binInvalidRepository.save(invalid);
    }
}

