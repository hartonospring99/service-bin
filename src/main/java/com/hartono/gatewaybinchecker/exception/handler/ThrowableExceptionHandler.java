package com.hartono.gatewaybinchecker.exception.handler;

import com.hartono.gatewaybinchecker.exception.ThrowableException;
import com.hartono.gatewaybinchecker.util.ExceptionUtil;
import io.vertx.core.json.JsonObject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Provider
public class ThrowableExceptionHandler implements ExceptionMapper<ThrowableException> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThrowableExceptionHandler.class);

	@Override
	public Response toResponse(ThrowableException ex) {
		try {
			JsonObject obj = new JsonObject(ex.getMessage());
			LOGGER.error("Internal Server Error : \n{}", ExceptionUtil.exceptionStandard(obj));
			return Response.status(Response.Status.BAD_REQUEST).entity(obj.getJsonObject("respBody").encode()).build();
		} catch (Exception e) {
			LOGGER.error("Internal Server Error : \n{}", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}