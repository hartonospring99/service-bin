package com.hartono.gatewaybinchecker.exception.handler;

import com.hartono.gatewaybinchecker.util.ExceptionUtil;
import io.vertx.core.json.JsonObject;
import jakarta.ws.rs.core.NoContentException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class NoContentExceptionHandler implements ExceptionMapper<NoContentException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(NoContentExceptionHandler.class);

    @Override
    public Response toResponse(NoContentException exception) {
    	JsonObject obj = new JsonObject(exception.getMessage());
        LOGGER.error("No Content: \n{}", ExceptionUtil.exceptionStandard(obj));
        return Response.status(Response.Status.NO_CONTENT).entity(obj.getJsonObject("respBody").encode()).build();
    }
}