package com.hartono.gatewaybinchecker.exception.handler;

import com.hartono.gatewaybinchecker.exception.ValidationApiException;
import com.hartono.gatewaybinchecker.util.ExceptionUtil;
import io.vertx.core.json.JsonObject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Provider
public class ValidationExceptionHandler implements ExceptionMapper<ValidationApiException> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationExceptionHandler.class);

	@Override
	public Response toResponse(ValidationApiException exception) {
		JsonObject obj = new JsonObject(exception.getMessage());
		LOGGER.error("Validation Error : \n{}", ExceptionUtil.exceptionStandard(obj));
		return Response.status(Response.Status.BAD_REQUEST).entity(obj.getJsonObject("respBody").encode()).build();
	}
}