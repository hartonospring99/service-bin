package com.hartono.gatewaybinchecker.exception;

public class ThrowableException extends Exception {
    public ThrowableException(String obj) {
        super(obj);
    }
}
