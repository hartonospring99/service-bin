package com.hartono.gatewaybinchecker.exception;

public class NoContentException extends Exception {

	public NoContentException(String msg) {
		super(msg);
	}
}