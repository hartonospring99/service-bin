package com.hartono.gatewaybinchecker.exception;

public class ValidationApiException extends Exception {
    public ValidationApiException(String str) {
        super(str);
    }
}