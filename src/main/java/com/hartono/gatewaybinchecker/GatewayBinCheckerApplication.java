package com.hartono.gatewaybinchecker;

import com.hartono.gatewaybinchecker.services.InitBin;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayBinCheckerApplication {

	private InitBin initBin;

	public GatewayBinCheckerApplication(InitBin initBin) {
		this.initBin = initBin;
	}


	public static void main(String[] args) {
		SpringApplication.run(GatewayBinCheckerApplication.class, args);
	}

	@Bean
	public CommandLineRunner CommandLineRunnerBean() {
		return (args) -> {
			initBin.initBinInValid();
			initBin.initBinValid();
		};
	}
}
