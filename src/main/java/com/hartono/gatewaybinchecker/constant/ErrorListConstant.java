package com.hartono.gatewaybinchecker.constant;

import io.vertx.core.json.JsonObject;

public class ErrorListConstant {
    private ErrorListConstant() {
        throw new IllegalStateException("Utility class");
    }

    public static final JsonObject SERVER_ERROR = gen("INTERNAL_SERVER_ERROR", "Internal Server Error");
    public static final JsonObject INVALID_BIN = gen("INVALID_BIN", "Invalid Bin");
    public static final JsonObject SUSPENDED_API_KEY = gen("SUSPENDED_API_KEY", "Suspended API Key");
    public static final JsonObject API_KEY_LIMIT = gen("API_KEY_LIMIT", "API Usage Limit Exceeded");
    public static final JsonObject ERROR_WEB_BIN = gen("ERROR_WEB_BIN", "Error Call Web BIN");

    private static JsonObject gen(String errCode, String message) {
        return new JsonObject().put("error_code", errCode).put("message", message);
    }
}
