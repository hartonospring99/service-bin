package com.hartono.gatewaybinchecker.constant;

public class ConstantVariable {
    private ConstantVariable(){
		//do nothing
	}

    public static final String DATETIME_FULL_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
}
