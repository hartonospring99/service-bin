package com.hartono.gatewaybinchecker.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Entity
@Data
@Setter
@Getter
@ToString
@Table(name = "bin_invalid")
public class BinInvalid {

    @Id
    @Column(name = "bin", nullable = false)
    private String bin;

}
