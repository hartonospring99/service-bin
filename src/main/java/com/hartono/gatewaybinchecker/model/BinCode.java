
package com.hartono.gatewaybinchecker.model;

import io.vertx.core.json.JsonObject;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;
import java.util.function.Consumer;

@Entity
@Data
@Setter
@Getter
@ToString
@Table(name = "bin")
public class BinCode extends JsonObject {
    @Id
    @Column(name = "bin", nullable = false)
    private String bin;

    @Column(name = "bank")
    private String bank = "";

    @Column(name = "card")
    private String card = "";

    @Column(name = "bin_type")
    private String type = "";

    @Column(name = "bin_level")
    private String level = "";

    @Column(name = "country")
    private String country = "";

    @Column(name = "country_code")
    private String countryCode = "";

    @Column(name = "website")
    private String website = "";

    @Column(name = "phone")
    private String phone = "";

    @Column(name = "bin_valid")
    private String valid = "";

    @Override
    public void forEach(Consumer<? super Map.Entry<String, Object>> action) {
        super.forEach(action);
    }
}
