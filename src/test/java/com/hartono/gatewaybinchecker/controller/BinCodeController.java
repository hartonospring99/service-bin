package com.hartono.gatewaybinchecker.controller;

import com.hartono.gatewaybinchecker.exception.ThrowableException;
import com.hartono.gatewaybinchecker.services.BinCodeService;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.vertx.core.json.JsonObject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BinCodeController {

    @Autowired
    BinCodeService binCodeService;

    @GET
    @Path("/v1/bin/{bin}")
    public Response findBinCode(@Context UriInfo uriInfo,
                                @PathVariable("bin") String bin)
            throws ServerErrorException,
            UnirestException,
            ThrowableException {

        JsonObject resp = binCodeService.getBinCode(uriInfo, bin);
        return Response.ok(resp.toString()).build();
    }
}
